
const Tutorial = require('./../model/tutorialModel')

const getAllTutorial = async (req, res) => {

    try {
        const response = await Tutorial.getAll(req.query.title);
        res.status(200).json(response);
    } catch (err) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const getTutorialById = async (req, res) => {
    try {
        const response = await Tutorial.getById(req.params.id);
        res.status(200).json(response);
    } catch (err) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const postNewTutorial = async (req, res) => {

    const tutorial = new Tutorial(req.body)
    try {
        const response = await tutorial.create();
        res.status(201).json(response);
    } catch (err) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const putTutorial = async (req, res) => {
    try {
        const newTutorial = new Tutorial(req.body)
        const response = await newTutorial.update(req.params.id);
        res.status(200).json(response);

    } catch (err) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const deleteTutorialById = async (req, res) => {
    try {
        const response = await Tutorial.deleteById(req.params.id);

        res.status(200).json(response);
    } catch (err) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const deleteTutorial = async (req, res) => {
    try {
        const response = await Tutorial.deleteAll();
        res.status(200).json(response);
    } catch (err) {
        res.status(500).json({ error: 'Internal server error' });
    }
}

const getAllPublished = async (req, res) => {
    try {
        const response = await Tutorial.getPublished();
        res.status(200).json(response);
    } catch (err) {
        res.status(500).json({ error: 'Internal server error' });
    }
}


module.exports = {
    getAllTutorial,
    postNewTutorial,
    getTutorialById,
    putTutorial,
    deleteTutorialById,
    deleteTutorial,
    getAllPublished,
}