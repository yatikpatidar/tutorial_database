const express = require('express')
const { getAllTutorial,
    postNewTutorial,
    getTutorialById,
    putTutorial,
    deleteTutorialById,
    deleteTutorial,
    getAllPublished,
} = require('./../controller/tutorialController.js')

const router = express.Router()

router.get('/', getAllTutorial)
router.post('/', postNewTutorial)
router.get('/published' , getAllPublished)
router.get('/:id', getTutorialById)
router.put('/:id', putTutorial)
router.delete('/:id', deleteTutorialById)
router.delete('/', deleteTutorial)
router.get('/' , getAllTutorial)


module.exports = router