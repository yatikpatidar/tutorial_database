const pool = require('./../db')

class Tutorial {
    constructor({ title, description, published }) {
        this.title = title;
        this.description = description;
        this.published = published;
    }

    static async getById(inputId) {
        try {
            const response = await pool.query("SELECT * FROM tutorials WHERE id = $1", [inputId]);
            return response.rows;

        } catch (error) {
            throw new Error(err.message);
        }
    }

    static async getAll(inputTitle) {
        let getQuery = 'SELECT * FROM tutorials';

        if (inputTitle) {
            getQuery += ` WHERE title LIKE '%${inputTitle}%'`
        }

        try {
            const response = await pool.query('SELECT * FROM tutorials');
            return response.rows

        } catch (err) {
            throw new Error(err.message);
        }
    }

    static async deleteById(inputId) {
        try {
            const response = await pool.query("DELETE FROM tutorials WHERE id = $1 RETURNING *", [inputId]);
            return response.rows;

        } catch (error) {
            throw new Error(err.message);
        }
    }

    static async deleteAll() {
        try {
            const response = await pool.query("DELETE FROM tutorials RETURNING *");
            return response.rows;

        } catch (error) {
            throw new Error(err.message);
        }
    }

    static async getPublished() {
        try {
            const response = await pool.query("SELECT * FROM tutorials WHERE published=true");
            return response.rows;

        } catch (error) {
            throw new Error(err.message);
        }
    }

    async create() {
        try {
            const response = await pool.query("INSERT INTO tutorials (title , description , published ) VALUES($1 ,$2 , $3) RETURNING *", [this.title, this.description, this.published = (this.published === undefined ? false : this.published)]);
            return response.rows;

        } catch (err) {
            throw new Error(err.message);
        }
    }

    async update(inputId) {
        try {
            const response = await pool.query("UPDATE tutorials SET title = $1 , description = $2 , published = $3 WHERE id = $4 RETURNING *", [this.title, this.description, this.published, inputId]);
            return response.rows;

        } catch (err) {
            throw new Error(err.message);
        }
    }

}

module.exports = Tutorial
