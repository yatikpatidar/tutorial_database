const express = require('express')
const tutorials = require('./routes/tutorial.js')


const app = express()
const port = 3050

app.use(express.json())

app.use('/api/tutorials' , tutorials )


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})